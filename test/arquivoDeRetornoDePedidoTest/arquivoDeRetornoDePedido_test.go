package arquivoDeRetornoDePedidoTest

import (
	"layout-infarma/arquivoDeRetornoDePedido"
	"strconv"
	"testing"
)

func TestCabecalhoFaltas(t *testing.T){
	numCnpj := "10224587000176"
	verLayout := "1.01"
	numPedVenCnv, _ := strconv.ParseInt("12482", 10, 32)
	numPedVen := int32(numPedVenCnv)
	codPedCli := "15142"

	cabecalho := arquivoDeRetornoDePedido.CabecalhoFaltas(numCnpj, verLayout, numPedVen, codPedCli) //entrar com os dados do banco

	if len(cabecalho) != 38 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if cabecalho != "011.01  102245870001760151420000012482" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestDetalheFaltas(t *testing.T) {
	codPrdCli := "1111"
	qtdAte, _ := strconv.ParseInt("50", 10, 32)
	prcUnitario, _ := strconv.ParseInt("14", 10, 32)

	detalhes:= arquivoDeRetornoDePedido.DetalheFaltas( codPrdCli, int32(qtdAte), float32(prcUnitario))

	if len(detalhes) != 34 {
		t.Error("Detalhes não tem o tamanho adequado")
	}else{
		if detalhes != "0200000000001111000005000000001400" {
			t.Error("Detalhes não é compativel")
		}
	}
}

func TestRodapeFaltas(t *testing.T)  {
	qtdIte, _ := strconv.ParseInt("4", 10, 32)
	qtdUnd, _ := strconv.ParseInt("15", 10, 32)
	vlrTotal, _ := strconv.ParseFloat("11.26", 10)

	rodape := arquivoDeRetornoDePedido.RodapeFaltas( int32(qtdIte), int32(qtdUnd), float32(vlrTotal))

	if len(rodape) != 27 {
		t.Error("Rodapé não tem o tamanho adequado")
	}else {
		if rodape != "090004000000001500000001126" {
			t.Error("Detalhes não é compativel")
		}
	}
}