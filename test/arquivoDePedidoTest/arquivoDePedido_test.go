package arquivoDePedidoTest

import (
	"layout-infarma/arquivoDePedido"
	"os"
	"strconv"
	"testing"
)

// teste para um Pedido sem a politica de comercializacao
func TestGetStruct01(t *testing.T) {

	file, err := os.Open("../fileTest/21141904.PED")
	if err != nil {
		t.Error("não abriu arquivo", err)}

	arquivo := arquivoDePedido.GetStruct(file)

	//REGISTRO TIPO 1
	var registro1 arquivoDePedido.RegistroTipo01
	registro1.IdentificadorDoTipoDeRegistro = 01
	registro1.VersaoDoLayoutDoArquivo = "1.02"
	registro1.CnpjDoCliente = "15749976001887"
	registro1.CodigoDoClienteNaDistribuidora = 3852
	registro1.NumeroDoPedidoNoCliente = "001141904"
	registro1.DataDoPedido = 20171009
	registro1.HoraDoPedido = 1453

	if registro1.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo01.IdentificadorDoTipoDeRegistro {
		t.Error("Identificação do tipo e registro não é compativel")
	}

	if registro1.VersaoDoLayoutDoArquivo != arquivo.RegistroTipo01.VersaoDoLayoutDoArquivo {
		t.Error("Versão do layout não é compativel")
	}

	if registro1.CnpjDoCliente != arquivo.RegistroTipo01.CnpjDoCliente {
		t.Error("Cnpj do cliente não é compativel")
	}

	if registro1.CodigoDoClienteNaDistribuidora != arquivo.RegistroTipo01.CodigoDoClienteNaDistribuidora {
		t.Error("Cogigo do cliente na distribuidoranão é compativel")
	}

	if registro1.NumeroDoPedidoNoCliente != arquivo.RegistroTipo01.NumeroDoPedidoNoCliente {
		t.Error("Numero do pedido no cliente não é compativel")
	}

	if registro1.DataDoPedido != arquivo.RegistroTipo01.DataDoPedido {
		t.Error("Data do pedido não é compativel")
	}

	if registro1.HoraDoPedido != arquivo.RegistroTipo01.HoraDoPedido {
		t.Error("Hora do pedido não é compativel")
	}

	//REGISTRO TIPO 2
	var registro2 arquivoDePedido.RegistroTipo02
	registro2.IdentificadorDoTipoDeRegistro = 02
	registro2.Mensagem = ""

	if registro2.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo02[0].IdentificadorDoTipoDeRegistro {
		t.Error("Identificador do tipo de registro não é compativel")
	}

	if registro2.Mensagem != arquivo.RegistroTipo02[0].Mensagem {
		t.Error(" Mensagem não é compativel")
	}

	//REGISTRO TIPO 3
	codigoDoProduto, _ := strconv.ParseInt("7896714203690", 10, 64)
	quatidade,  _ := strconv.ParseInt("1", 10, 32)

	var registro3  arquivoDePedido.RegistroTipo03
	registro3.IdentificadorDoTipoDeRegistro   = 03
	registro3.TipoDeCodigoDoProduto = 2
	registro3.CodigoDoProduto = int64(codigoDoProduto)
	registro3.Quantidade = int32(quatidade)

	if registro3.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo03[0].IdentificadorDoTipoDeRegistro {
		t.Error("Identificador do tipo de registro  não é compativel")
	}

	if registro3.TipoDeCodigoDoProduto != arquivo.RegistroTipo03[0].TipoDeCodigoDoProduto {
		t.Error("Tipo o codigo do produto não é compativel")
	}

	if registro3.CodigoDoProduto != arquivo.RegistroTipo03[0].CodigoDoProduto {
		t.Error("Codigo do produto não é compativel:", registro3.CodigoDoProduto, " != " , arquivo.RegistroTipo03[0].CodigoDoProduto )
	}

	if registro3.Quantidade != arquivo.RegistroTipo03[0].Quantidade {
		t.Error("Quantidade não é compativel:", registro3.Quantidade, " != ", arquivo.RegistroTipo03[0].Quantidade )
	}

	//REGISTRO TIPO 9
	identificadorDoTipoDeRegistro, _ := strconv.ParseInt("09", 10, 32)

	var registro9 arquivoDePedido.RegistroTipo09
	registro9.IdentificadorDoTipoDeRegistro = int32(identificadorDoTipoDeRegistro)
	registro9.QuantidadeTotalDeItens = 14
	registro9.QuantidadeTotalDeUnidades  =  43

	if registro9.IdentificadorDoTipoDeRegistro != arquivo.RegistroTipo09.IdentificadorDoTipoDeRegistro {
		t.Error("Identificador do tipo de registro não é compativel")
	}

	if registro9.QuantidadeTotalDeItens != arquivo.RegistroTipo09.QuantidadeTotalDeItens {
		t.Error("Quantidade não é compativel")
	}

	if registro9.QuantidadeTotalDeUnidades != arquivo.RegistroTipo09.QuantidadeTotalDeUnidades {
		t.Error("Quantidade total de unidades não é compativel")
	}
}

//// teste para um registro com politica no cabecalho
func TestGetStruct02(t *testing.T) {

	file2, err := os.Open("../fileTest/21141904.PED")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo2 := arquivoDePedido.GetStruct(file2)

	var registro102 arquivoDePedido.RegistroTipo01
	registro102.IdentificadorDoTipoDeRegistro = 01
	registro102.VersaoDoLayoutDoArquivo = "1.02"
	registro102.CnpjDoCliente = "15749976001887"
	registro102.CodigoDoClienteNaDistribuidora = 3852
	registro102.NumeroDoPedidoNoCliente = "001141904"
	registro102.DataDoPedido = 20171009
	registro102.HoraDoPedido = 1453
	registro102 .ChaveDaPoliticaDeComercializacao = 2222
	registro102 .CodigoDaTabelaDePrazo = 333

	//testes para o registro 1 - cabeçalho com politicas de comercialização
	if registro102.IdentificadorDoTipoDeRegistro != arquivo2.RegistroTipo01.IdentificadorDoTipoDeRegistro {
		t.Error("Identificação do tipo e registro não é compativel")
	}

	if registro102.VersaoDoLayoutDoArquivo != arquivo2.RegistroTipo01.VersaoDoLayoutDoArquivo {
		t.Error("Versão do layout não é compativel")
	}

	if registro102.CnpjDoCliente != arquivo2.RegistroTipo01.CnpjDoCliente {
		t.Error("Cnpj do cliente não é compativel")
	}

	if registro102.CodigoDoClienteNaDistribuidora != arquivo2.RegistroTipo01.CodigoDoClienteNaDistribuidora {
		t.Error("Cogigo do cliente na distribuidora não é compativel")
	}

	if registro102.NumeroDoPedidoNoCliente != arquivo2.RegistroTipo01.NumeroDoPedidoNoCliente {
		t.Error("Numero do pedido no cliente não é compativel")
	}

	if registro102.DataDoPedido != arquivo2.RegistroTipo01.DataDoPedido {
		t.Error("Data do pedido não é compativel")
	}

	if registro102.HoraDoPedido != arquivo2.RegistroTipo01.HoraDoPedido {
		t.Error("Hora do pedido não é compativel")
	}

	if registro102.ChaveDaPoliticaDeComercializacao != arquivo2.RegistroTipo01.ChaveDaPoliticaDeComercializacao {
		t.Error("Chave da politica de comercialização não é compativel")
	}

	if registro102.CodigoDaTabelaDePrazo != arquivo2.RegistroTipo01.CodigoDaTabelaDePrazo {
		t.Error("Codigo da tabela de prazo não é compativel")
	}

	if registro102.CnpjDoFornecedor != arquivo2.RegistroTipo01.CnpjDoFornecedor {
		t.Error("CNPJ do Fornecevdor não é compativel")
	}

	//REGISTRO TIPO 3
	codigoDoProduto2, _ := strconv.ParseInt("7896714203690", 10, 64)
	quatidade2,  _ := strconv.ParseInt("1", 10, 32)
	chaveDaPoliticaDeComercializacao2,  _ := strconv.ParseInt("2222", 10, 32)

	var registro3  arquivoDePedido.RegistroTipo03
	registro3.IdentificadorDoTipoDeRegistro   = 03
	registro3.TipoDeCodigoDoProduto = 2
	registro3.CodigoDoProduto = int64(codigoDoProduto2)
	registro3.Quantidade = int32(quatidade2)
	registro3.ChaveDaPoliticaDeComercializacao = int32(chaveDaPoliticaDeComercializacao2)   // tem que ajustar de arcodo com o arquivo

	if registro3.IdentificadorDoTipoDeRegistro != arquivo2.RegistroTipo03[0].IdentificadorDoTipoDeRegistro {
		t.Error("Identificadordo tipo de registro  não é compativel")
	}

	if registro3.TipoDeCodigoDoProduto != arquivo2.RegistroTipo03[0].TipoDeCodigoDoProduto {
		t.Error("Tipo o codigo do produto não é compativel")
	}

	if registro3.CodigoDoProduto != arquivo2.RegistroTipo03[0].CodigoDoProduto {
		t.Error("Codigo do produto não é compativel:", registro3.CodigoDoProduto, " != " , arquivo2.RegistroTipo03[0].CodigoDoProduto )
	}

	if registro3.Quantidade != arquivo2.RegistroTipo03[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}

	if registro3.ChaveDaPoliticaDeComercializacao != arquivo2.RegistroTipo03[0].ChaveDaPoliticaDeComercializacao {
		t.Error("Chave da politica não é compativel", registro3.ChaveDaPoliticaDeComercializacao, " != " , arquivo2.RegistroTipo03[0].ChaveDaPoliticaDeComercializacao )
	}
}







