package arquivoDeRetornoDeNotaFiscalTest

import (
	"layout-infarma/arquivoDeRetornoDeNotaFiscal"
	"testing"
	"time"
)

func TestHeaderDoArquivo(t *testing.T){
	verLayout := "1.01"
	numPdConv := "124812"
	codPedCli := "000001"
	str := "2014-11-12T11:45:26.371Z"
	datPedArq, _ := time.Parse(time.RFC3339, str)

	header := arquivoDePedidoRetornoNotaFiscal.HeaderDoArquivo(verLayout, datPedArq, numPdConv, codPedCli) //entrar com os dados do banco

	if len(header) != 22 {
		t.Error("Cabecalho não tem o tamanho adeguado")
	}else{
		if header != "001.01  20141112124812" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestHeaderDaNotaFiscal(t *testing.T){
	numNota := 278960
	codCfo1 := 1
	str := "2014-11-12T11:45:26.371Z"
	datEmissao, _ := time.Parse(time.RFC3339, str)
	codPedido := 278960
	vlrLiqItens := 15.21
	vlrIPI := 15.21
	vlrDescontoCom := 1.00
	vlrDscBon := 1.00
	vlrDscTri := 1.00
	vlrRepIcms := 15.21
	vlrBasSubsTrib := 15.21
	vlrSubsTrib := 15.21
	vlrTotalNota := 15.21
	vlrBasIcmsTri := 15.21
	vlrIcmsTri := 15.21

	//entrar com os dados do banco
	var header = arquivoDePedidoRetornoNotaFiscal.HeaderDaNotaFiscal(int32(numNota), int32(codCfo1), datEmissao, int32(codPedido),
		float32(vlrLiqItens), float32(vlrIPI), float32(vlrDescontoCom), float32(vlrDscBon), float32(vlrDscTri), float32(vlrRepIcms),
		float32(vlrBasSubsTrib), float32(vlrSubsTrib), float32(vlrTotalNota), float32(vlrBasIcmsTri), float32(vlrIcmsTri))

	if len(header) != 143 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if header != "012789601        201411120000278960000000001521000000001521000000000300000000001521000000001521000000001521000000001521000000001521000000001521" {
			t.Error("Cabecalho não é compativel")
		}
	}
}

func TestItensDaNotaFiscal(t *testing.T){

	numNota := 278960
	codProduto := 4
	codEan := "7898183600406"
	qtdProduto := 20
	qtdBonificacao := 2
	prcUnitario := 100.56
	perDesconto := 3.98
	alqIcms := 2.01
	sequencial := 1

	//entrar com os dados do banco
	var itens = arquivoDePedidoRetornoNotaFiscal.ItensDaNotaFiscal(int32(numNota), int32(codProduto), codEan, int32(qtdProduto), int32(qtdBonificacao), float32(prcUnitario), float32(perDesconto), float32(alqIcms), int8(sequencial))

	if len(itens) != 56 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if itens != "022789600000047898183600406 0000220000000100560398020101" {
			t.Error("Cabecalho não é compativel")
		}
	}
}


func TestTitulos(t *testing.T){

	numDocumento := "278960"
	parDocumento  := "2"
	str := "2014-11-12T11:45:26.371Z"
	datEmissao, _ := time.Parse(time.RFC3339, str)
	datVenciento,_ := time.Parse(time.RFC3339, str)
	valorDocumento := 45.00
	vlrDscAtc  := 12.45
	datDscAtc := time.Time{}

	//entrar com os dados do banco
	var itens = arquivoDePedidoRetornoNotaFiscal.Titulos(numDocumento, parDocumento, datEmissao, datVenciento, float32(valorDocumento), float32(vlrDscAtc), datDscAtc )

	if len(itens) != 60 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if itens != "082789602   201411122014111200000000450000000000124500010101" {
			t.Error("Cabecalho não é compativel")
		}
	}
}


func TestTotalizador(t *testing.T){

	totalItens :=  1
	unidades := 5

	//entrar com os dados do banco
	var itens = arquivoDePedidoRetornoNotaFiscal.Totalizador(int32(totalItens), int32(unidades))

	if len(itens) != 16 {
		t.Error("Cabecalho não tem o tamanho adequado")
	}else{
		if itens != "0900010000000500" {
			t.Error("Cabecalho não é compativel")
		}
	}
}
