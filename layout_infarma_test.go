package layout_infarma_test

import (
	"layout-infarma"
	"os"
	"testing"
)

func TestGetArquivoDePedido(t *testing.T) {
	f, err := os.Open("test/fileTest/12345678.PED")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq := layout_infarma.GetArquivoDePedido(f)

	if arq.RegistroTipo01.CnpjDoCliente == "" {
		t.Errorf("CNPJ não foi lido %s", arq.RegistroTipo01.CnpjDoCliente)
	} else {
		t.Logf("CNPJ lido: %s", arq.RegistroTipo01.CnpjDoCliente)
	}
}

func TestGetArquivoDeRetornoFaltasCabecalho(t *testing.T) {
	header := layout_infarma.GetArquivoDeRetornoFaltasCabecalho("10224587000176","1.01",12482,"15142")

	if header == "" {
		t.Error("Não gerou nada")
	} else {
		t.Logf("Resultado :%s" , header)
	}
}

func TestGetArquivoDeRetornoFaltasDetalhe(t *testing.T) {
	detalhes := layout_infarma.GetArquivoDeRetornoFaltasDetalhe("1111",50,14)

	if detalhes == "" {
		t.Error("Não gerou nada")
	} else {
		t.Logf("Resultado :%s" , detalhes)
	}
}

func TestGetArquivoDeRetornoFaltasRodape(t *testing.T) {
	rodape := layout_infarma.GetArquivoDeRetornoFaltasRodape(4,15,11.26)

	if rodape == "" {
		t.Error("Não gerou nada")
	} else {
		t.Logf("Resultado :%s" , rodape)
	}
}