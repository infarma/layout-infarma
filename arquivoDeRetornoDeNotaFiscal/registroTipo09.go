package arquivoDePedidoRetornoNotaFiscal

// Totalizador
type RegistroTipo09 struct {
	IdentificadorDoTipoDeRegistro      int32 `json:"IdentificadorDoTipoDeRegistro"`
	QuantidadeTotalDeItensDoArquivo    int32 `json:"QuantidadeTotalDeItensDoArquivo"`
	QuantidadeTotalDeUnidadesNoArquivo int32 `json:"QuantidadeTotalDeUnidadesNoArquivo"`
}
