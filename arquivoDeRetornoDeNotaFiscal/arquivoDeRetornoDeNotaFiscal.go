package arquivoDePedidoRetornoNotaFiscal

import (
	"fmt"
	"layout-infarma/utils"
	"strconv"
	"strings"
	"time"
)

// Arquivo XML padrão ou layout abaixo
type ArquivoDeRotornoDeNotaFiscal struct {
	RegistroTipo00 RegistroTipo00   `json:"RegistroTipo00"`
	RegistroTipo01 RegistroTipo01   `json:"RegistroTipo01"`
	RegistroTipo02 []RegistroTipo02 `json:"RegistroTipo02"`
	RegistroTipo08 []RegistroTipo08 `json:"RegistroTipo08"`
	RegistroTipo09 RegistroTipo09   `json:"RegistroTipo09"`
}

// GeraRetornoNotasInfarma
//Registro Tipo 0 - Header do Arquivo
func HeaderDoArquivo(verLayout string, datPedArq time.Time, numPdConv string, codPedCli string) string {
	cabecalho := fmt.Sprint("00")
	cabecalho += fmt.Sprint(verLayout)
	cabecalho += fmt.Sprint("  ")
	cabecalho += fmt.Sprint(datPedArq.Format("20060102"))

	if strings.TrimSpace(verLayout) == "1.01" {
		if numPdConv != "" {
			numPdConvInt, _ := strconv.ParseInt(numPdConv, 10, 32)
			if numPdConvInt > 0 {
				cabecalho += fmt.Sprintf("%06d", utils.ConvertStringToInt(numPdConv))
			} else {
				cabecalho += fmt.Sprintf("%06d", utils.ConvertStringToInt(codPedCli))
			}
		} else {
			cabecalho += fmt.Sprintf("%06d", utils.ConvertStringToInt(codPedCli))
		}
	} else {
		if numPdConv != "" {
			numPdConvInt, _ := strconv.ParseInt(numPdConv, 10, 32)
			if numPdConvInt > 0 {
				cabecalho += fmt.Sprintf("%09d", utils.ConvertStringToInt(numPdConv))
			} else {
				cabecalho += fmt.Sprintf("%09d", utils.ConvertStringToInt(codPedCli))
			}
		} else {
			cabecalho += fmt.Sprintf("%09d", utils.ConvertStringToInt(codPedCli))
		}
	}
	return cabecalho
}

//Registro Tipo 1 - Header da Nota Fiscal
func HeaderDaNotaFiscal(numNota int32, codCfo1 int32, datEmissao time.Time, codPedido int32, vlrLiqItens float32, vlrIPI float32,
	vlrDescontoCom float32, vlrDscBon float32, vlrDscTri float32, vlrRepIcms float32, vlrBasSubsTrib float32, vlrSubsTrib float32,
	vlrTotalNota float32, vlrBasIcmsTri float32, vlrIcmsTri float32) string {
	cabecalhoNota := fmt.Sprint("01")
	cabecalhoNota += fmt.Sprintf("%06d", numNota)
	cabecalhoNota += fmt.Sprintf("%-9d", codCfo1)
	cabecalhoNota += fmt.Sprint(datEmissao.Format("20060102"))
	cabecalhoNota += fmt.Sprintf("%010d", codPedido)
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrLiqItens*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrIPI*100))

	valorDesconto := vlrDescontoCom + vlrDscBon + vlrDscTri
	cabecalhoNota += fmt.Sprintf("%012d", int32(valorDesconto*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrRepIcms*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrBasSubsTrib*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrSubsTrib*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrTotalNota*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrBasIcmsTri*100))
	cabecalhoNota += fmt.Sprintf("%012d", int32(vlrIcmsTri*100))

	return cabecalhoNota
}

//Registro Tipo 2 - Itens da Nota Fiscal
func ItensDaNotaFiscal(numNota int32, codProduto int32, codEan string, qtdProduto int32, qtdBonificacao int32, prcUnitario float32,
	perDesconto float32, alqIcms float32, sequencial int8) string {
	itens := fmt.Sprint("02")
	itens += fmt.Sprintf("%06d", numNota)
	itens += fmt.Sprintf("%06d", codProduto)
	itens += fmt.Sprintf("%-14d", utils.ConvertStringToInt(codEan))

	quantidade := qtdProduto + qtdBonificacao
	itens += fmt.Sprintf("%06d", quantidade)

	itens += fmt.Sprintf("%012d", int32(prcUnitario*100))
	itens += fmt.Sprintf("%04d", int32(perDesconto*100))
	itens += fmt.Sprintf("%04d", int32(alqIcms*100))
	itens += fmt.Sprintf("%02d", sequencial)

	return itens
}

//Registro Tipo 08 - Títulos
func Titulos(numDocumento string, parDocumento string, datEmissao time.Time, datVenciento time.Time, valorDocumento float32,
	vlrDscAtc float32, datDscAtc time.Time) string {
	titulos := fmt.Sprint("08")

	numeroNota := numDocumento + parDocumento

	titulos += fmt.Sprintf("%-10d", utils.ConvertStringToInt(numeroNota))
	titulos += fmt.Sprint(datEmissao.Format("20060102"))
	titulos += fmt.Sprint(datVenciento.Format("20060102"))
	titulos += fmt.Sprintf("%012d", int32(valorDocumento*100))
	titulos += fmt.Sprintf("%012d", int32(vlrDscAtc*100))
	titulos += fmt.Sprint(datDscAtc.Format("20060102"))

	return titulos
}

//Registro Tipo 09 - Totalizador
func Totalizador(sequencial int32, unidades int32) string {
	totalizador := fmt.Sprint("09")
	totalizador += fmt.Sprintf("%04d", sequencial)
	totalizador +=  fmt.Sprintf("%010d", unidades*100)
	return totalizador
}
