package arquivoDePedidoRetornoNotaFiscal

// Itens da Nota Fiscal
type RegistroTipo02 struct {
	TipoRegistro        int32 `json:"TipoRegistro"`
	NumeroDaNotaFiscal  int32 `json:"NumeroDaNotaFiscal"`
	CodigoDoProduto     int32 `json:"CodigoDoProduto"`
	CodigoDeBarras      int32 `json:"CodigoDeBarras"`
	Quantidade          int32 `json:"Quantidade"`
	PrecoUnitario       int32 `json:"PrecoUnitario"`
	Desconto            int32 `json:"Desconto"`
	PercentualDeIcms    int32 `json:"PercentualDeIcms"`
	SequenciaNotaFiscal int32 `json:"SequenciaNotaFiscal"`
}
