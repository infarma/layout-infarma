package arquivoDePedidoRetornoNotaFiscal

// Titulos
type RegistroTipo08 struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	NumeroDoTitulo     string `json:"NumeroDoTitulo"`
	DataDeEmissao      int32  `json:"DataDeEmissao"`
	DataDeVencimento   int32  `json:"DataDeVencimento"`
	ValorDoTitulo      int32  `json:"ValorDoTitulo"`
	ValorDoDesconto    int32  `json:"ValorDoDesconto"`
	ValidadeDoDesconto int32  `json:"ValidadeDoDesconto"`
}
