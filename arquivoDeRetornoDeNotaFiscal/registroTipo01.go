package arquivoDePedidoRetornoNotaFiscal

// Header da Nota Fiscal
type RegistroTipo01 struct {
	TipoRegistro                  int8   `json:"TipoRegistro"`
	NumeroDaNotaFiscal            int32  `json:"NumeroDaNotaFiscal"`
	CodigoFiscalDeOperacao        string `json:"CodigoFiscalDeOperacao"`
	DataDaEmissao                 int32  `json:"DataDaEmissao"`
	NumeroDoPedidoNoDistribuidor  int32  `json:"NumeroDoPedidoNoDistribuidor"`
	ValorDaMercadoria             int32  `json:"ValorDaMercadoria"`
	ValorDoIpi                    int32  `json:"ValorDoIpi"`
	ValorDoDesconto               int32  `json:"ValorDoDesconto"`
	ValorDoRepasse                int32  `json:"ValorDoRepasse"`
	BaseDeCalculoDoImpostoRetido  int32  `json:"BaseDeCalculoDoImpostoRetido"`
	ValorDoImpostoRetido          int32  `json:"ValorDoImpostoRetido"`
	ValorTotalDaNotaFiscal        int32  `json:"ValorTotalDaNotaFiscal"`
	BaseDeCalculoDoIcmsTributavel int32  `json:"BaseDeCalculoDoIcmsTributavel"`
	ChaveDeAcesso                 string `json:"ChaveDeAcesso"`
}
