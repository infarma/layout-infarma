package arquivoDePedidoRetornoNotaFiscal

// Header do Arquivo
type RegistroTipo00 struct {
	TipoRegistro            int32  `json:"TipoRegistro"`
	VersaoDoLayoutDoArquivo string `json:"VersaoDoLayoutDoArquivo"`
	DataDaGravacao          int32  `json:"DataDaGravacao"`
	NumeroDoPedidoNoCliente int32  `json:"NumeroDoPedidoNoCliente"`
	CnpjDoFornecedor        int32  `json:"CnpjDoFornecedor"`
}