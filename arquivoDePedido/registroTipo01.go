package arquivoDePedido

import (
	"strconv"
	"strings"
)

// Identificação do Pedido
type RegistroTipo01 struct {
	IdentificadorDoTipoDeRegistro    int32  `json:"IdentificadorDoTipoDeRegistro"`
	VersaoDoLayoutDoArquivo          string `json:"VersaoDoLayoutDoArquivo"`
	CnpjDoCliente                    string `json:"CnpjDoCliente"`
	CodigoDoClienteNaDistribuidora   int32  `json:"CodigoDoClienteNaDistribuidora"`
	NumeroDoPedidoNoCliente          string `json:"NumeroDoPedidoNoCliente"`
	DataDoPedido                     int32  `json:"DataDoPedido"`
	HoraDoPedido                     int32  `json:"HoraDoPedido"`
	ChaveDaPoliticaDeComercializacao int32  `json:"ChaveDaPoliticaDeComercializacao"`
	CodigoDaTabelaDePrazo            int32  `json:"CodigoDaTabelaDePrazo"`
	CnpjDoFornecedor                 string `json:"CnpjDoFornecedor"`
}

func GetRegistroTipo1(runes []rune) RegistroTipo01 {
	registroTipo01 := RegistroTipo01{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 32)
	registroTipo01.IdentificadorDoTipoDeRegistro = int32(identificador)

	registroTipo01.VersaoDoLayoutDoArquivo = strings.TrimSpace(string(runes[2:8]))

	cnpjCliente := string(runes[8:22])
	registroTipo01.CnpjDoCliente = cnpjCliente

	codigoCliente, _ := strconv.ParseInt(string(runes[22:28]), 10, 32)
	registroTipo01.CodigoDoClienteNaDistribuidora = int32(codigoCliente)

	if registroTipo01.VersaoDoLayoutDoArquivo == "1.01" {
		registroTipo01.NumeroDoPedidoNoCliente = string(runes[28:34])

		dataPedido, _ := strconv.ParseInt(string(runes[34:42]), 10, 32)
		registroTipo01.DataDoPedido = int32(dataPedido)

		horaPedido, _ := strconv.ParseInt(string(runes[42:46]), 10, 32)
		registroTipo01.HoraDoPedido = int32(horaPedido)

		if len(runes) > 46 {
			chave, _ := strconv.ParseInt(string(runes[46:50]), 10, 32)
			registroTipo01.ChaveDaPoliticaDeComercializacao = int32(chave)

			codigoTabela, _ := strconv.ParseInt(string(runes[50:53]), 10, 32)
			registroTipo01.CodigoDaTabelaDePrazo = int32(codigoTabela)

			cnpjFornecedor := string(runes[53:67])
			registroTipo01.CnpjDoFornecedor = cnpjFornecedor
		}
	} else if registroTipo01.VersaoDoLayoutDoArquivo == "1.02" {
		registroTipo01.NumeroDoPedidoNoCliente =  string(runes[28:37])

		dataPedido, _ := strconv.ParseInt(string(runes[37:45]), 10, 32)
		registroTipo01.DataDoPedido = int32(dataPedido)

		horaPedido, _ := strconv.ParseInt(string(runes[45:49]), 10, 32)
		registroTipo01.HoraDoPedido = int32(horaPedido)

		if len(runes) > 49 {
			chave, _ := strconv.ParseInt(string(runes[49:53]), 10, 32)
			registroTipo01.ChaveDaPoliticaDeComercializacao = int32(chave)

			codigoTabela, _ := strconv.ParseInt(string(runes[53:56]), 10, 32)
			registroTipo01.CodigoDaTabelaDePrazo = int32(codigoTabela)
		}
	}

	return registroTipo01
}