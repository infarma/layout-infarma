package arquivoDePedido

import (
	"strconv"
)

// Totalizador
type RegistroTipo09 struct {
	IdentificadorDoTipoDeRegistro int32 `json:"IdentificadorDoTipoDeRegistro"`
	QuantidadeTotalDeItens        int32 `json:"QuantidadeTotalDeItens"`
	QuantidadeTotalDeUnidades     int32 `json:"QuantidadeTotalDeUnidades"`
}

func GetRegistroTipo9(runes []rune) RegistroTipo09 {
	registroTipo09 := RegistroTipo09{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 32)
	registroTipo09.IdentificadorDoTipoDeRegistro = int32(identificador)

	totalItens, _ := strconv.ParseInt(string(runes[2:6]), 10, 32)
	registroTipo09.QuantidadeTotalDeItens = int32(totalItens)

	totalQtd, _ := strconv.ParseInt(string(runes[6:14]), 10, 32)
	registroTipo09.QuantidadeTotalDeUnidades = int32(totalQtd)

	return registroTipo09
}
