package arquivoDePedido

import (
	"strconv"
)

// Itens do Pedido
type RegistroTipo03 struct {
	IdentificadorDoTipoDeRegistro    int32 `json:"IdentificadorDoTipoDeRegistro"`
	TipoDeCodigoDoProduto            int32 `json:"TipoDeCodigoDoProduto"`
	CodigoDoProduto                  int64 `json:"CodigoDoProduto"`
	Quantidade                       int32 `json:"Quantidade"`
	ChaveDaPoliticaDeComercializacao int32 `json:"ChaveDaPoliticaDeComercializacao"`
}

func GetRegistroTipo3(runes []rune) RegistroTipo03 {
	registroTipo03 := RegistroTipo03{}

	identificador, _ := strconv.ParseInt(string(runes[0:2]), 10, 32)
	registroTipo03.IdentificadorDoTipoDeRegistro = int32(identificador)

	tipoProduto, _ := strconv.ParseInt(string(runes[2:3]), 10, 32)
	registroTipo03.TipoDeCodigoDoProduto = int32(tipoProduto)

	if registroTipo03.TipoDeCodigoDoProduto == 1 {
		codigoInterno, _ := strconv.ParseInt(string(runes[3:16]), 10, 64)
		registroTipo03.CodigoDoProduto = int64(codigoInterno)
	} else {
		codigoInterno, _ := strconv.ParseInt(string(runes[3:16]), 10, 64)
		registroTipo03.CodigoDoProduto = int64(codigoInterno)
	}

	quantidade, _ := strconv.ParseInt(string(runes[16:23]), 10, 32)
	registroTipo03.Quantidade = int32(quantidade)

	if len(runes) > 23 {
		chave, _ := strconv.ParseInt(string(runes[23:27]), 10, 32)
		registroTipo03.ChaveDaPoliticaDeComercializacao = int32(chave)
	}

	return registroTipo03
}