package arquivoDePedido

import (
	"bufio"
	"os"
)

// Arquivo de Pedido
type ArquivoDePedido struct {
	RegistroTipo01 RegistroTipo01   `json:"RegistroTipo01"`
	RegistroTipo02 []RegistroTipo02 `json:"RegistroTipo02"`
	RegistroTipo03 []RegistroTipo03 `json:"RegistroTipo03"`
	RegistroTipo09 RegistroTipo09   `json:"RegistroTipo09"`
}

func GetStruct(fileHandle *os.File) ArquivoDePedido {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		if identificador == "01" {
			arquivo.RegistroTipo01 = GetRegistroTipo1(runes)
		} else if identificador == "02" {
			arquivo.RegistroTipo02 = append(arquivo.RegistroTipo02, GetRegistroTipo2(runes))
		} else if identificador == "03" {
			arquivo.RegistroTipo03 = append(arquivo.RegistroTipo03, GetRegistroTipo3(runes))
		} else if identificador == "09" {
			arquivo.RegistroTipo09 = GetRegistroTipo9(runes)
		}
	}
	return arquivo
}
