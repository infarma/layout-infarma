package utils

import (
	"fmt"
	"regexp"
	"strconv"
)

func RemoveLetrasECaracteres(codPedCli string) string {
	re := regexp.MustCompile("[0-9]+")
	str := re.FindAllString(fmt.Sprint(codPedCli), -1)

	retorno := ""
	for i := 0; i < len(str); i++ {
		retorno += str[i]
	}
	return retorno
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}