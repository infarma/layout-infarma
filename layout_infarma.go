package layout_infarma

import (
	"bitbucket.org/infarma/layout-infarma/arquivoDePedido"
	"bitbucket.org/infarma/layout-infarma/arquivoDeRetornoDePedido"
	"os"
)

const filtro = "*.PED"

func GetArquivoDePedido(fileHandle *os.File) arquivoDePedido.ArquivoDePedido {
	return arquivoDePedido.GetStruct(fileHandle)
}

func GetArquivoDeRetornoFaltasCabecalho(numCnpj string, verLayout string, numPedVen int32, codPedCli string) string {
	return arquivoDeRetornoDePedido.CabecalhoFaltas(numCnpj, verLayout, numPedVen, codPedCli)
}

func GetArquivoDeRetornoFaltasDetalhe(codPrdCli string, qtdAte int32, prcUnitario float32) string {
	return arquivoDeRetornoDePedido.DetalheFaltas(codPrdCli, qtdAte, prcUnitario)
}

func GetArquivoDeRetornoFaltasRodape(qtdIte int32, qtdUnd int32, vlrTotal float32) string {
	return arquivoDeRetornoDePedido.RodapeFaltas(qtdIte, qtdUnd, vlrTotal)
}