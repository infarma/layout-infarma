package arquivoDeRetornoDePedido

// Totalizador
type RegistroTipo09 struct {
	IdentificadorDoTipoDeRegistro int32 `json:"IdentificadorDoTipoDeRegistro"`
	QuantidadeTotalDeItens        int32 `json:"QuantidadeTotalDeItens"`
	QuantidadeTotalDeUnidades     int32 `json:"QuantidadeTotalDeUnidades"`
	TotalLiquidoDoPedido          int32 `json:"TotalLiquidoDoPedido"`
}
