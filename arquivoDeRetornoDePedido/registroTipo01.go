package arquivoDeRetornoDePedido

// Identificação do Cliente / Pedido
type RegistroTipo01 struct {
	IdentificadorDoTipoDeRegistro int32  `json:"IdentificadorDoTipoDeRegistro"`
	VersaoDoLayoutDoArquivo       string `json:"VersaoDoLayoutDoArquivo"`
	CnpjDoCliente                 int32  `json:"CnpjDoCliente"`
	NumeroDoPedidoNoCliente       int32  `json:"NumeroDoPedidoNoCliente"`
	NumeroDoPedidoNoDistribuidor  int32  `json:"NumeroDoPedidoNoDistribuidor"`
	CnpjDoFornecedor              int32  `json:"CnpjDoFornecedor"`
}
