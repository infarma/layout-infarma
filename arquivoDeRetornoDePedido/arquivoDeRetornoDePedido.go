package arquivoDeRetornoDePedido

import (
	"fmt"
	"strconv"
	"strings"
)

// Arquivo de Retorno de Pedido - Itens Atendidos
type ArquivoDeRotornoDePedido struct {
	RegistroTipo01 RegistroTipo01   `json:"RegistroTipo01"`
	RegistroTipo02 []RegistroTipo02 `json:"RegistroTipo02"`
	RegistroTipo09 RegistroTipo09   `json:"RegistroTipo09"`
}

// Registro Tipo 1 - Identificacoes de Cliente/Pedido
func CabecalhoFaltas(numCnpj string, verLayout string, numPedVen int32, codPedCli string) string {
	cabecalho := fmt.Sprint("01")
	cabecalho += fmt.Sprintf("%s", verLayout)
	cabecalho += fmt.Sprint("  ")
	cabecalho += fmt.Sprintf("%s", numCnpj)

	codPedCliInt, _ := strconv.ParseInt(codPedCli, 10, 32)

	if strings.TrimSpace(verLayout) == "1.01" {
		cabecalho += fmt.Sprintf("%06d", codPedCliInt)
	} else {
		cabecalho += fmt.Sprintf("%09d", codPedCliInt)
	}

	cabecalho += fmt.Sprintf("%010d", int32(numPedVen))

	return cabecalho
}

// Registro Tipo 2 - Itens Atendidos
func DetalheFaltas(codPrdCli string, qtdAte int32, prcUnitario float32) string {
	detalhes := fmt.Sprint("02")
	detalhes += fmt.Sprintf("%014s", codPrdCli)
	detalhes += fmt.Sprintf("%07d", qtdAte)
	detalhes += fmt.Sprintf("%011s", strings.Replace(fmt.Sprintf("%.2f", prcUnitario), ".", "", 1))

	return detalhes
}

// Registro Tipo 9 - Totalizador
func RodapeFaltas(qtdIte int32, qtdUnd int32, vlrTotal float32) string {
	rodape := fmt.Sprint("09")
	rodape += fmt.Sprintf("%04d", qtdIte)
	rodape += fmt.Sprintf("%010d", qtdUnd)
	rodape += fmt.Sprintf("%011s", strings.Replace(fmt.Sprintf("%.2f", vlrTotal), ".", "", 1))

	return rodape
}
