package arquivoDeRetornoDePedido

// Itens Atendidos
type RegistroTipo02 struct {
	IdentificadorDoTipoDeRegistro int32 `json:"IdentificadorDoTipoDeRegistro"`
	TipoDeCodigoDoProduto         int32 `json:"TipoDeCodigoDoProduto"`
	QuantidadeDoProdutoAtendido   int32 `json:"QuantidadeDoProdutoAtendido"`
	PrecoUnitarioProduto          int32 `json:"PrecoUnitarioProduto"`
}
